if ! pictures_dir="$(xdg-user-dir PICTURES 2> /dev/null )"
then
    pictures_dir="$HOME/Pictures"
fi

screenshot_folder="${pictures_dir:-$HOME/Pictures}/bullet_holes"

re='^[0-9]+$'

usage()
{
    printf "%s\n\t%s\n\t%s\n\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n" \
        "OUSMA - Ouch you shot me, Asshole! (screen \"shot\", get it?)" \
        "a screenshot wrapper I wrote because I could" \
        "Usage: ousma [OPTIONS]... [FILE]" \
        "-h     --help                  display this message" \
        "-s     --snag                  select area of screen" \
        "-b     --backend  [backend]    choose backend(scrot*, maim, xwd, escrotum)" \
        "-c     --clip                  to clipboard" \
        "-u     --upload                uploads a screenshot" \
        "-w     --wait     [time]       waits given seconds before screenshot"
}

rm -f /tmp/ousma_tmp.png

generatename()
{
    if [ -z "$shotname" ]; then
        month=$(date +"%B_%Y")
        shottime=$(date +"%a_%d_%H_%M_%S")

        if [ ! -d "$screenshot_folder/$month" ]; then
            mkdir -p "$screenshot_folder/$month"
        fi
    
        if [ "$clipboard" = true ]; then
            shotname="/tmp/ousma_tmp.png"
        else
            shotname="$screenshot_folder/$month/$shottime".png 
        fi
    fi
}

scrotshot()
{
    generatename

    if [ "$selection" = "true" ]; then
        scrot -s "$shotname"
    else
        scrot "$shotname"
    fi
}

maimshot()
{
    generatename

    if [ "$selection" = "true" ]; then
        maim -s "$shotname"
    else
        maim "$shotname"
    fi
}

xwdshot()
{
    generatename

    if [ "$selection" = "true" ]; then
        xwd > /tmp/temp.xwd
    else
        xwd -root > /tmp/temp.xwd
    fi
    convert /tmp/temp.xwd "$shotname"
    rm /tmp/temp.xwd
}

escrotumshot()
{
    generatename

    if [ "$selection" = "true" ]; then
        echo "Nothing yet"
    else
        echo "Nothing yet"
    fi
}

# backends order scrot, maim, xwd, escrotum
screenshot()
{
    if [ -z "$backend" ]; then
        for cmd in scrot maim xwd escrotum
        do
            if command -v "$cmd" $> /dev/null
            then
                backend="$cmd"
                break
            fi
        done
    else
        if ! command -v "$backend" $> /dev/null; then
            echo "command $backend doesn't exist!"
            exit 1 
        fi
    fi

    case "$backend" in
        scrot )
            scrotshot
            ;;
        maim )
            maimshot
            ;;
        xwd )
            xwdshot
            ;;
        escrotum )
            echo "escrotum will be used"
            ;;
        * )
            echo "no supported backend found"
            exit 1
            ;;
    esac

    if [ ! -d "$screenshot_folder" ]; then
        mkdir -p "$screenshot_folder"
    fi
}

while [ ! "$#" -eq 0 ]; do
    case "$1" in
        -h|--help )
            usage
            exit
            ;;
        -b|--backend )
            if [ "${2:0:1}" = "-" ] || [ -z "$2" ]; then
                echo "expected backend"
                exit 1
            fi
            backend=$2
            shift
            ;;
        -s|--snag )
            selection=true
            ;;
        -c|--clip )
            clipboard=true
            ;;
        -u|--upload )
            upload=true
            clipboard=true
            ;;
        -w|--wait )
        	if [ "${2:0:1}" = "-" ] || [ -z "$2" ] || [[ ! $2 =~ $re ]]; then
        		echo "Expected number after $1"
        		usage
        		exit 1
        	fi
        	wait=$2
        	shift
        	;;
        * )
            if [ "${1:0:1}" = "-" ] || [ ! -z "$2" ]; then
                echo "Unexpected option: $1"
                usage
                exit 1
            fi
            shotname="$1"
            ;;
    esac
    shift
done

if [[ ! -z ${wait} ]]; then
	sleep "$wait"
fi

screenshot

if [ "$clipboard" = "true" ]; then
    if command -v xclip $> /dev/null; then
        if [ "$upload" = "true" ]; then
            echo "$(curl -sF"file=@$shotname" http://0x0.st)" | xclip -selection clipboard
        else
            xclip -selection clipboard -t image/png -i "$shotname"
        fi
    else
        echo "Sorry can't copy picture, I need xclip"
    fi
fi

if [ "$temporary" = "true" ]; then
    rm "$shotname"
fi
