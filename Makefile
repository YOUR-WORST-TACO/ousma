destdir		?=
prefix 		?= /usr/local
debug 		?= false

bin/%:
	mkdir -p bin
	echo "#!/usr/bin/env bash" > "$@"
	@if [ "$(debug)" = "true" ]; then \
		echo "set -x" >> "$@"; \
	fi
	cat src/license_head >> "$@"
	cat src/ousma.sh >> "$@"
	chmod +x "$@"

all: bin/ousma

devel: clean
	$(MAKE) debug=true bin/ousma

install: bin/ousma
	install -D bin/ousma \
		"$(destdir)/$(prefix)/bin/ousma"
clean:
	rm -rf bin
