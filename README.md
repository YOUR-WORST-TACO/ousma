# OUSMA - Ouch you shot me, Asshole!

My own take on a screenshot wrapper because I didnt want to use the other ones.

## ousma -h
```
OUSMA - Ouch you shot me, Asshole! (screen "shot", get it?)
	a screenshot wrapper I wrote because I could
	Usage: ousma [OPTIONS]... [FILE]

	-h     --help                  display this message
	-s     --snag                  select area of screen
	-b     --backend  [backend]    choose backend(scrot*, maim, xwd, escrotum)
	-c     --clip                  to clipboard
	-t     --temp                  used with clipboard, doesnt save screenshot
	-u     --upload                uploads a screenshot

```

## Installing
just type the following

```
make
sudo make install
```

it installs to /usr/share/bin by default
to change this behavior use
```
make prefix=/usr
sudo make install
```

## todo

### general
- [x] take screenshots
- [x] select screenshots
- [x] store screenshots
- [x] screenshot to clipboard

### backends
- [x] scrot
- [x] maim
- [x] xwd
- [ ] escrotum

### extras
- [x] sort by date?
- [x] upload to 0x0
- [ ] sort by content?
- [ ] some editing like post production, add watermark?
